package test.java.hybridFramework;

import java.io.IOException;

import org.testng.annotations.Test;

import main.java.hybridFramework.pageObjects.LoginPage;
import main.java.hybridFramework.testBase.TestBase;

public class Login extends TestBase{
	

	@Test
	public void login() throws IOException, InterruptedException
	{
		LoginPage loginPage = new LoginPage(driver);
		loginPage.clickLogInLink();
	}
}

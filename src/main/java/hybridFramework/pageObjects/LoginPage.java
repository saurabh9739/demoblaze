package main.java.hybridFramework.pageObjects;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import main.java.hybridFramework.waitHelper.WaitHelper;

public class LoginPage {
	
	WebDriver driver;
	Properties properties;
    WaitHelper waitHelper;
    
	 public LoginPage(WebDriver driver) throws IOException
	    {
	        this.driver = driver;
	        PageFactory.initElements(driver, this);
	        waitHelper = new WaitHelper(driver);
	        properties = new Properties();
	        loadLoginConfigFile();
	        
	    }

	    public void loadLoginConfigFile() throws IOException
	    {
	   	    File loginFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\config\\login.properties");
			FileInputStream orFileInputStream = new FileInputStream(loginFile);
			properties.load(orFileInputStream);
	    }
	    
	    public void loadPaymentConfigFile() throws IOException
	    {
	    	File paymentFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\com\\hybridFramework\\config\\payment.properties");
	    	FileInputStream paymentInputStream = new FileInputStream(paymentFile);
	    	properties.load(paymentInputStream);
	    }
	    
	    @FindBy(xpath="//*[@id='login2']")
	    private WebElement logInLinkOnHomePage;
	    
	    @FindBy(id="dnn_ctr959_Login_txtUsername")
	    private WebElement userNameTextFieldOnHomePage;

	    @FindBy(id="dnn_ctr959_Login_txtPassword")
	    private WebElement passwordTextFieldOnHomePage;
	    
	    @FindBy(id= "dnn_ctr959_Login_cmdLogin")
	    private WebElement loginButtonOnHomePage;

	    
	    public void clickLogInLink() throws InterruptedException
	    {
	    	Thread.sleep(5000);
	    	logInLinkOnHomePage.click();
	    }
}

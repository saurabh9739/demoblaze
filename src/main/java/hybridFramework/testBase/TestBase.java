package main.java.hybridFramework.testBase;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import main.java.hybridFramework.waitHelper.WaitHelper;

public class TestBase {

	public Properties properties;
	public static WebDriver driver;
	WaitHelper waitHelper;
	Calendar calendar;
	SimpleDateFormat formater; 
	public static ExtentReports extent;
	public static ExtentTest test;
	//public Excel_reader excelreader;
	public ITestResult result;
	
	String timeOfReport; 
	 {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss");
		timeOfReport = formater.format(calendar.getTime());
		//extent = new ExtentReports(System.getProperty("user.dir") + "\\src\\main\\java\\hybridFramework\\extentReports\\" + timeOfReport + ".html", false);
		extent = new ExtentReports(System.getProperty("user.dir") + "\\src\\main\\java\\hybridFramework\\extentReports\\" +"Report"+".html", false);
	}
	
	public void loadConfigPropertiesFile() throws IOException
	{
		properties = new Properties();
		File orFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\config\\config.properties");
		//File orFile = new File("C:\\Users\\Tripathi.Saurabh\\eclipse-workspace\\PersonifyWeb\\src\\main\\java\\hybridFramework\\config\\config.properties");
		FileInputStream orFileInputStream = new FileInputStream(orFile);
		properties.load(orFileInputStream);
		
		/*File registrationFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\config\\registration.properties");
		FileInputStream orFileInputStream = new FileInputStream(registrationFile);
		properties.load(orFileInputStream);*/
	}
	 
	public void getBrowser() throws IOException
	{
		System.out.println(System.getProperty("os.name"));
		if(System.getProperty("os.name").contains("Windows"))
		{
			System.out.println(System.getProperty("user.dir"));
			loadConfigPropertiesFile();
			System.out.println(properties.getProperty("Browser"));
			//String browser = properties.getProperty("Browser");
			if(properties.getProperty("Browser").equalsIgnoreCase("chrome"))
			{
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\drivers\\chromeDriver\\chromedriver.exe");
				driver = new ChromeDriver();
			}
			
			else if(properties.getProperty("Browser").equalsIgnoreCase("firefox"))
			{
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\drivers\\firefoxDriver\\geckodriver.exe");
				driver = new FirefoxDriver();
			}
		}
		
		else if(System.getProperty("os.name").contains("Mac"))
		{
			System.out.println(System.getProperty("os.name"));
			if(properties.getProperty("Browser").equalsIgnoreCase("firefox")){
				System.out.println(System.getProperty("user.dir"));
				System.setProperty("webdriver.gecko.driver", System.getProperty("user.dir")+"/drivers/geckodriver");
				driver = new FirefoxDriver();
			}
			else if(properties.getProperty("Browser").equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/drivers/chromedriver");
				driver = new ChromeDriver();
			}
		}
	}
	
	
	public void getresult(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.SUCCESS) {

			test.log(LogStatus.PASS, result.getName() + " test is pass");
			String screen = getScreenShot(result.getName());
			test.log(LogStatus.PASS, test.addScreenCapture(screen));
		} else if (result.getStatus() == ITestResult.SKIP) {
			test.log(LogStatus.SKIP, result.getName() + " test is skipped and  reason is:- " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.FAILURE) {
			test.log(LogStatus.FAIL, result.getName() + " test is failed and reason is:- " + result.getThrowable());
			String screen = getScreenShot(result.getName());
			test.log(LogStatus.FAIL, test.addScreenCapture(screen));
		} /*else if (result.getStatus() == ITestResult.STARTED) {
			test.log(LogStatus.INFO, result.getName() + " test is started");
		}*/
	}
	
	public String getScreenShot(String testName) throws IOException 
	{
		calendar = Calendar.getInstance();
		formater = new SimpleDateFormat("dd_MM_yyyy_hh_mm");
		File image = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String imagelocation = System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\screenshots\\";
		String actualImageName = imagelocation+"_"+formater.format(calendar.getTime())+".png";
		File destFile = new File(actualImageName);
		FileUtils.copyFile(image, destFile);
		return actualImageName;
	}
	
	@BeforeTest
	public void launchBrowser() throws IOException, InterruptedException
	{
		getBrowser();
		/*Set<String> windows  = driver.getWindowHandles();
		for(String window : windows)
		{
			driver.switchTo().window(window);
			driver.manage().window().maximize();
			driver.get(properties.getProperty("URL"));
		}*/
		driver.manage().window().maximize();
		driver.get(properties.getProperty("URL"));
		
		waitHelper = new WaitHelper(driver);
		waitHelper.setPageLoadTimeout(properties.getProperty("PageLoadTimeOut"), TimeUnit.SECONDS);	
		/*System.out.println("browser maximized");
		driver.get(properties.getProperty("URL"));
		System.out.println("hit URL");*/
		
		//waitHelper.setImplicitWait(properties.getProperty("ImplcitWait"), TimeUnit.SECONDS);
		
	}
	
	@BeforeMethod
	public void beforeMethod(Method result) {
		//System.out.println(result.getName());
		String testname = result.getName();
		test = extent.startTest(testname);
		test.log(LogStatus.INFO, result.getName() + " test Started");
		System.out.println(result.getName() + " test Started");
	}
	
	@AfterMethod 
	public void afterMethod(ITestResult result) throws IOException {
		getresult(result);
	}

	@AfterTest(alwaysRun = true)
	public void afterTest() {
		//driver.quit();
		extent.endTest(test);
		extent.flush();
	}
	
	@AfterSuite
	public void afterSuite()
	{
		//sendReportToRecipient();
	}
}
package main.java.hybridFramework.waitHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitHelper {

	private WebDriver driver;
	   
    Properties properties; 
    WebDriverWait wait;
    
	public WaitHelper(WebDriver driver) throws IOException 
	{
		this.driver = driver;
		loadConfigFile();
		String explicitTimeoutValue = properties.getProperty("ExplicitTimeOutValue");
	    int explicitTimeout = Integer.parseInt(explicitTimeoutValue);  
	    wait = new WebDriverWait(driver, explicitTimeout);
	}
	
	public void loadConfigFile() throws IOException
    {
		properties = new Properties();
		File orFile = new File(System.getProperty("user.dir")+"\\src\\main\\java\\hybridFramework\\config\\config.properties");
		FileInputStream orFileInputStream = new FileInputStream(orFile);
		properties.load(orFileInputStream);
    }
	
	public void setImplicitWait(String timeoutValue, TimeUnit unit) 
	{
		long timeout = Integer.parseInt(timeoutValue);
		driver.manage().timeouts().implicitlyWait(timeout, unit == null ? TimeUnit.SECONDS : unit);
	}
	
	public void setPageLoadTimeout(String timeoutValue, TimeUnit unit) 
	{
		long timeout = Integer.parseInt(timeoutValue);
		driver.manage().timeouts().pageLoadTimeout(timeout, unit == null ? TimeUnit.SECONDS : unit);
	}
	
	
	public void waitForVisibilityOfElement(WebElement element) throws InterruptedException 
	{
		wait.until(ExpectedConditions.visibilityOf(element));
		Thread.sleep(2000);
	}
	
	public void waitForElementToBeClickable(WebElement webElement) throws InterruptedException
	{
		wait.until(ExpectedConditions.elementToBeClickable(webElement));
		Thread.sleep(2000);
	}
	
}

